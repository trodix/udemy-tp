import firebase from 'firebase'
import firestore from 'firebase/firestore'
// Initialize Firebase
var config = {
    apiKey: "AIzaSyAWT5Pek0c_xSP5EwaCabiOb27hOUcK0aE",
    authDomain: "udemy-geo-ninjas-90767.firebaseapp.com",
    databaseURL: "https://udemy-geo-ninjas-90767.firebaseio.com",
    projectId: "udemy-geo-ninjas-90767",
    storageBucket: "",
    messagingSenderId: "621810642953"
};
const firebaseApp = firebase.initializeApp(config);

export default firebaseApp.firestore()