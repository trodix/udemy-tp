import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Initialize Firebase
var config = {
    apiKey: "AIzaSyDFfeM3dLFcQveh0YRGdNfostwiqJIyAS0",
    authDomain: "udemy-ninja-chat-685ec.firebaseapp.com",
    databaseURL: "https://udemy-ninja-chat-685ec.firebaseio.com",
    projectId: "udemy-ninja-chat-685ec",
    storageBucket: "udemy-ninja-chat-685ec.appspot.com",
    messagingSenderId: "375618715012"
};
const firebaseApp = firebase.initializeApp(config);

export default firebaseApp.firestore()