import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Initialize Firebase
var config = {
    apiKey: "AIzaSyD3nI1SSAGdjNUZ7_TTXMUR-o3F9py3sS0",
    authDomain: "udemy-ninja-smoothies-129bb.firebaseapp.com",
    databaseURL: "https://udemy-ninja-smoothies-129bb.firebaseio.com",
    projectId: "udemy-ninja-smoothies-129bb",
    storageBucket: "udemy-ninja-smoothies-129bb.appspot.com",
    messagingSenderId: "58557868840"
};

const firebaseApp = firebase.initializeApp(config);
// firebaseApp.firestore().settings({ timestampsInSnapshots: true })

export default firebaseApp.firestore()